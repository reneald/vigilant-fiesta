package be.leanderonline.cscg.api.characters;

import be.leanderonline.cscg.api.CscgModelAssembler;
import be.leanderonline.cscg.api.dtos.CharacterDto;
import be.leanderonline.cscg.model.characters.Character;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class CharacterModelAssembler implements RepresentationModelAssembler<Character, EntityModel<CharacterDto>>, CscgModelAssembler {
    @Override
    @NonNull
    public EntityModel<CharacterDto> toModel(@NonNull Character entity) {
        CharacterDto dto = CharacterMapper.INSTANCE.toDto(entity);
        EntityModel<CharacterDto> entityModel = EntityModel.of(dto,
                linkTo(methodOn(CharacterController.class).one(entity.getId())).withSelfRel(),
                linkTo(methodOn(CharacterController.class).all()).withRel(IanaLinkRelations.COLLECTION));
        addHomeRel(entityModel);
        return entityModel;
    }

    @Override
    @NonNull
    public CollectionModel<EntityModel<CharacterDto>> toCollectionModel(Iterable<? extends Character> entities) {
        List<EntityModel<CharacterDto>> entityModels = new ArrayList<>();
        for (Iterator<? extends Character> iterator = entities.iterator(); iterator.hasNext(); ) {
            Character entity = iterator.next();
            EntityModel<CharacterDto> entityModel = this.toModel(entity);
            entityModels.add(entityModel);
        }
        CollectionModel<EntityModel<CharacterDto>> collectionModel = CollectionModel.of(entityModels,
                linkTo(methodOn(CharacterController.class).all()).withSelfRel());
        addHomeRel(collectionModel);
        return collectionModel;
    }
}
