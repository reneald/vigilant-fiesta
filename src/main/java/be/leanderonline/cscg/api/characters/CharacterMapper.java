package be.leanderonline.cscg.api.characters;

import be.leanderonline.cscg.api.dtos.CharacterDto;
import be.leanderonline.cscg.model.characters.Character;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CharacterMapper {
    CharacterMapper INSTANCE = Mappers.getMapper( CharacterMapper.class );

    Character toEntity(CharacterDto dto);

    @Mapping(target = "id", ignore = true)
    CharacterDto toDto(Character entity);
}
