package be.leanderonline.cscg.api.characters;

import be.leanderonline.cscg.api.dtos.CharacterDto;
import be.leanderonline.cscg.model.characters.Character;
import be.leanderonline.cscg.model.characters.CharacterService;
import be.leanderonline.cscg.model.exceptions.CharacterNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/characters")
public class CharacterController {
    private final CharacterService service;
    private final CharacterModelAssembler assembler;

    @Autowired
    CharacterController(CharacterService service, CharacterModelAssembler assembler) {
        this.service = service;
        this.assembler = assembler;
    }

    @PostMapping("")
    public @ResponseBody ResponseEntity<EntityModel<CharacterDto>> create(@RequestBody CharacterDto characterDto) {
        Character characterToCreate = new Character(characterDto.name());
        Character createdCharacter = service.createCharacter(characterToCreate);
        EntityModel<CharacterDto> characterEntityModel = assembler.toModel(createdCharacter);
        return ResponseEntity.created(characterEntityModel.getRequiredLink(IanaLinkRelations.SELF).toUri())
                .body(characterEntityModel);
    }

    @GetMapping("/{id}")
    public EntityModel<CharacterDto> one(@PathVariable("id") UUID id) {
        Character byId = service.findById(id).orElseThrow(() -> new CharacterNotFoundException("No character found for ID " + id));
        return assembler.toModel(byId);
    }

    @GetMapping("")
    public CollectionModel<EntityModel<CharacterDto>> all() {
        return assembler.toCollectionModel(service.findAll());
    }
}
