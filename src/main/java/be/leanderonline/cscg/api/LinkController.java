package be.leanderonline.cscg.api;

import be.leanderonline.cscg.api.characters.CharacterController;
import be.leanderonline.cscg.api.types.TypeController;
import org.springframework.hateoas.CollectionModel;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@RequestMapping("/links")
public class LinkController {

    @GetMapping("/")
    public ResponseEntity<CollectionModel<String>> getBasicLinks() {
        String message = "This resource's links section contains links to all collections that this API provides.";
        List<String> list = List.of(message);
        CollectionModel<String> contents = CollectionModel.of(list,
                linkTo(methodOn(LinkController.class).getBasicLinks()).withSelfRel(),
                linkTo(methodOn(CharacterController.class).all()).withRel("characters"),
                linkTo(methodOn(TypeController.class).all()).withRel("types"));
        return ResponseEntity.ok(contents);
    }
}
