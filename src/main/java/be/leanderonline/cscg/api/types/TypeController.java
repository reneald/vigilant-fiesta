package be.leanderonline.cscg.api.types;

import be.leanderonline.cscg.api.dtos.TypeDto;
import be.leanderonline.cscg.model.characters.types.MongoTypeRepository;
import be.leanderonline.cscg.model.exceptions.TypeNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/types")
public class TypeController {
    private final MongoTypeRepository repository;
    private final TypeModelAssembler assembler;

    @Autowired
    TypeController(MongoTypeRepository repository, TypeModelAssembler assembler) {
        this.repository = repository;
        this.assembler = assembler;
    }

    @GetMapping("/{id}")
    public EntityModel<TypeDto> one(@PathVariable("id") int id) {
        return assembler.toModel(repository.findById(id).orElseThrow(() -> new TypeNotFoundException("No type found for ID " + id)));
    }

    @GetMapping("")
    public CollectionModel<EntityModel<TypeDto>> all() {
        return assembler.toCollectionModel(repository.findAll());
    }
}
