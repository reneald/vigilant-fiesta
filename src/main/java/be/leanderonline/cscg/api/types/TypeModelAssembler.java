package be.leanderonline.cscg.api.types;

import be.leanderonline.cscg.api.CscgModelAssembler;
import be.leanderonline.cscg.api.dtos.TypeDto;
import be.leanderonline.cscg.model.characters.types.Type;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@Component
public class TypeModelAssembler  implements RepresentationModelAssembler<Type, EntityModel<TypeDto>>, CscgModelAssembler {
    @Override
    @NonNull
    public EntityModel<TypeDto> toModel(@NonNull Type entity) {
        TypeDto dto = TypeMapper.INSTANCE.toDto(entity);
        EntityModel<TypeDto> entityModel = EntityModel.of(dto,
                linkTo(methodOn(TypeController.class).one(entity.id())).withSelfRel(),
                linkTo(methodOn(TypeController.class).all()).withRel(IanaLinkRelations.COLLECTION));
        addHomeRel(entityModel);
        return entityModel;
    }

    @Override
    @NonNull
    public CollectionModel<EntityModel<TypeDto>> toCollectionModel(Iterable<? extends Type> entities) {
        List<EntityModel<TypeDto>> entityModels = new ArrayList<>();
        for (Type entity : entities) {
            EntityModel<TypeDto> entityModel = this.toModel(entity);
            entityModels.add(entityModel);
        }
        CollectionModel<EntityModel<TypeDto>> collectionModel = CollectionModel.of(entityModels,
                linkTo(methodOn(TypeController.class).all()).withSelfRel());
        addHomeRel(collectionModel);
        return collectionModel;
    }
}
