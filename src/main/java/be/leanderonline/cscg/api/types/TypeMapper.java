package be.leanderonline.cscg.api.types;

import be.leanderonline.cscg.api.dtos.TypeDto;
import be.leanderonline.cscg.model.characters.types.Type;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper
public interface TypeMapper {
    TypeMapper INSTANCE = Mappers.getMapper(TypeMapper.class);

    Type toEntity(TypeDto dto);

    @Mapping(target = "id", ignore = true)
    TypeDto toDto(Type entity);
}
