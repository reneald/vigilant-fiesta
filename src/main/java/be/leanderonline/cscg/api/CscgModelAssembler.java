package be.leanderonline.cscg.api;

import org.springframework.hateoas.IanaLinkRelations;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.RepresentationModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;

public interface CscgModelAssembler {

    default void addHomeRel(RepresentationModel<?> entityModel) {
        Link homeLink = WebMvcLinkBuilder.linkTo(WebMvcLinkBuilder.methodOn(LinkController.class).getBasicLinks()).withRel(IanaLinkRelations.CONTENTS);
        entityModel.add(homeLink);
    }
}
