package be.leanderonline.cscg.api.dtos;

import be.leanderonline.cscg.model.characters.stats.Statistics;

import java.util.UUID;

//@Data
public record CharacterDto(UUID id, Statistics statistics, String name) {

    public static class Builder {
        private UUID id;
        private Statistics statistics; //TODO change to StatisticsDto
        private String name;

        private Builder() {

        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder withId(UUID id) {
            this.id = id;
            return this;
        }

        public Builder withStatistics(Statistics statistics) {
            this.statistics = statistics;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public CharacterDto build() {
            return new CharacterDto(this.id, this.statistics, this.name);
        }
    }
}
