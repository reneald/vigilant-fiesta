package be.leanderonline.cscg.api.dtos;

public record TypeDto(Integer id, String name, int might, int speed, int intellect, int additionalPoints, int effort) {
}
