package be.leanderonline.cscg.model.characters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class CharacterService {
    private final MongoCharacterRepository repository;

    @Autowired
    public CharacterService(MongoCharacterRepository repository) {
        this.repository = repository;
    }

    public Character createCharacter(Character character) {
        return repository.insert(character);
    }

    public List<Character> findAll() {
        return repository.findAll();
    }
    public Optional<Character> findById(UUID id) {
        return repository.findById(id);
    }
}
