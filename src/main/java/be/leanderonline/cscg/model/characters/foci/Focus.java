package be.leanderonline.cscg.model.characters.foci;

import org.springframework.data.mongodb.core.mapping.Document;

@Document("foci")
public record Focus(int id, String name) {

    public static class Builder {
        private int id;
        private String name;

        private Builder() {

        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder of(Focus origin) {
            this.id = origin.id;
            this.name = origin.name;
            return this;
        }

        public Builder withId(int id) {
            this.id = id;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Focus build() {
            return new Focus(this.id, this.name);
        }
    }
}
