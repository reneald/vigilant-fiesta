package be.leanderonline.cscg.model.characters.types;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(Type.COLLECTION_NAME)
public record Type(int id, String name, int might, int speed, int intellect, int additionalPoints, int effort) {
    public static final String COLLECTION_NAME = "types";
    public static final int WARRIOR = 1;
    public static final int ADEPT = 2;
    public static final int EXPLORER = 3;
    public static final int SPEAKER = 4;

    public static class Builder {
        private int id;
        private String name;
        private int might;
        private int speed;
        private int intellect;
        private int additionalPoints;
        private int effort;

        private Builder() {}

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder of(Type origin) {
            this.id = origin.id;
            this.name = origin.name;
            this.might = origin.might;
            this.speed = origin.speed;
            this.intellect = origin.intellect;
            this.additionalPoints = origin.additionalPoints;
            this.effort = origin.effort;
            return this;
        }

        public Builder withId(int id) {
            this.id = id;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withMight(int might) {
            this.might = might;
            return this;
        }

        public Builder withSpeed(int speed) {
            this.speed = speed;
            return this;
        }

        public Builder withIntellect(int intellect) {
            this.intellect = intellect;
            return this;
        }

        public Builder withAdditionalPoints(int additionalPoints) {
            this.additionalPoints = additionalPoints;
            return this;
        }

        public Builder withEffort(int effort) {
            this.effort = effort;
            return this;
        }

        public Type build() {
            return new Type(this.id, this.name, this.might, this.speed, this.intellect, this.additionalPoints, this.effort);
        }
    }
}
