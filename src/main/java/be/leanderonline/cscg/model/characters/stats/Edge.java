package be.leanderonline.cscg.model.characters.stats;

public record Edge(int edge) {

    public static class Builder {
        private int edge;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder of(Edge origin) {
            this.edge = origin.edge;
            return this;
        }

        public Builder withEdge(int edge) {
            this.edge = edge;
            return this;
        }

        public Edge build() {
            return new Edge(this.edge);
        }
    }
}
