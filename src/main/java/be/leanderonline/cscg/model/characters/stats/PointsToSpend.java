package be.leanderonline.cscg.model.characters.stats;

public record PointsToSpend(int points) {

    public static class Builder {
        private int points;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder of(PointsToSpend origin) {
            this.points = origin.points;
            return this;
        }

        public Builder withPoints(int points) {
            this.points = points;
            return this;
        }

        public PointsToSpend build() {
            return new PointsToSpend(this.points);
        }
    }
}
