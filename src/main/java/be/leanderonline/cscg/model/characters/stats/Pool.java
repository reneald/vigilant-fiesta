package be.leanderonline.cscg.model.characters.stats;

public record Pool(int max, int current) {
    public Pool(int amount) {
        this(amount, amount);
    }

    public static class Builder {
        private int max;
        private int current;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder of(Pool origin) {
            this.max = origin.max;
            this.current = origin.current;
            return this;
        }

        public Builder withMax(int max) {
            this.max = max;
            return this;
        }

        public Builder withCurrent(int current) {
            this.current = current;
            return this;
        }

        public Pool build() {
            return new Pool(this.max, this.current);
        }
    }
}
