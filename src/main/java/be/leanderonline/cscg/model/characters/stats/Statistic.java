package be.leanderonline.cscg.model.characters.stats;

public record Statistic(StatisticType stat, Pool pool, Edge edge) {

    public static class Builder {
        private StatisticType stat;
        private Pool pool;
        private Edge edge;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder of(Statistic origin) {
            this.stat = origin.stat;
            this.pool = origin.pool;
            this.edge = origin.edge;
            return this;
        }

        public Builder withStat(StatisticType stat) {
            this.stat = stat;
            return this;
        }

        public Builder withPool(Pool pool) {
            this.pool = pool;
            return this;
        }

        public Builder withEdge(Edge edge) {
            this.edge = edge;
            return this;
        }

        public Statistic build() {
            return new Statistic(this.stat, this.pool, this.edge);
        }
    }
}
