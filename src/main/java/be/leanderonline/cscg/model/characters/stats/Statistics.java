package be.leanderonline.cscg.model.characters.stats;

import be.leanderonline.cscg.model.characters.types.Type;

public record Statistics(Statistic might, Statistic speed, Statistic intellect, PointsToSpend pointsToSpend) {
    public Statistics(Type type) {
        this(might(type), speed(type), intellect(type), pointsToSpend(type));
    }

    private static PointsToSpend pointsToSpend(Type type) {
        return new PointsToSpend(type.additionalPoints());
    }

    private static Statistic might(Type type) {
        return new Statistic(StatisticType.MIGHT, new Pool(type.might()), new Edge(0));
    }

    private static Statistic speed(Type type) {
        return new Statistic(StatisticType.SPEED, new Pool(type.speed()), new Edge(0));
    }
    private static Statistic intellect(Type type) {
        return new Statistic(StatisticType.INTELLECT, new Pool(type.intellect()), new Edge(0));
    }

    public static class Builder {
        Statistic might;
        Statistic speed;
        Statistic intellect;
        PointsToSpend pointsToSpend;

        private Builder() {
        }

        public static Builder newInstance() {
            return new Builder();
        }

        public Builder of(Statistics origin) {
            this.might = origin.might;
            this.speed = origin.speed;
            this.intellect = origin.intellect;
            this.pointsToSpend = origin.pointsToSpend;
            return this;
        }

        public Builder withMight(Statistic might) {
            if (!might.stat().equals(StatisticType.MIGHT)) {
                throw new IllegalArgumentException("Trying to use a " + might.stat() + " statistic as Might");
            }
            this.might = might;
            return this;
        }

        public Builder withSpeed(Statistic speed) {
            if (!speed.stat().equals(StatisticType.SPEED)) {
                throw new IllegalArgumentException("Trying to use a " + speed.stat() + " statistic as Speed");
            }
            this.speed = speed;
            return this;
        }

        public Builder withIntellect(Statistic intellect) {
            if (!intellect.stat().equals(StatisticType.INTELLECT)) {
                throw new IllegalArgumentException("Trying to use a " + intellect.stat() + " statistic as Intellect");
            }
            this.intellect = intellect;
            return this;
        }

        public Builder withPointsToSpend(PointsToSpend pointsToSpend) {
            this.pointsToSpend = pointsToSpend;
            return this;
        }

        public Statistics build() {
            return new Statistics(this.might, this.speed, this.intellect, this.pointsToSpend);
        }
    }
}
