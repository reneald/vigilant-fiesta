package be.leanderonline.cscg;

import be.leanderonline.cscg.common.UuidIdentifiedMongoRepositoryImpl;
import io.mongock.runner.springboot.EnableMongock;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.hateoas.config.EnableHypermediaSupport;

@Configuration
@EnableMongoRepositories(repositoryBaseClass = UuidIdentifiedMongoRepositoryImpl.class)
@EnableMongock
@EnableHypermediaSupport(type = EnableHypermediaSupport.HypermediaType.HAL)
public class CscgConfiguration {
}
