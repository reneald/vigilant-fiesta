package be.leanderonline.cscg.api;

import org.hamcrest.Matchers;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(LinkController.class)
class LinkControllerTest {
    @Autowired
    MockMvc mockMvc;

    @ParameterizedTest
    @ValueSource(strings = {"self", "characters", "types"})
    void getBasicLinksResponse_shouldContainAllNecessaryLinks(String linkName) throws Exception {
        // GIVEN nothing particular

        // WHEN/THEN
        mockMvc.perform(MockMvcRequestBuilders.get("/links/")
                .contentType("application/hal+json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._links", Matchers.hasKey(linkName)));
    }
}