package be.leanderonline.cscg.api.characters;

import be.leanderonline.cscg.api.dtos.CharacterDto;
import be.leanderonline.cscg.model.characters.Character;
import be.leanderonline.cscg.model.characters.CharacterService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(CharacterController.class)
class CharacterControllerTest {
    @Autowired
    MockMvc mockMvc;
    @MockBean
    private CharacterService service;
    @Autowired
    private CharacterModelAssembler assembler;
    @Autowired
    private ObjectMapper mapper;

    @ParameterizedTest
    @ValueSource(strings = {"self", "collection", "contents"})
    void one_whenIdFoundInRepository_thenShouldReturnOkWithCorrectType(String linkName) throws Exception {
        // GIVEN
        Character lea = new Character("Lea");
        UUID id = UUID.randomUUID();
        lea.setId(id);
        Mockito.when(service.findById(id)).thenReturn(Optional.of(lea));

        // WHEN
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/characters/" + id)
                .contentType("application/hal+json");

        // THEN
        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").doesNotExist())
                .andExpect(jsonPath("$.name", is("Lea")))
                .andExpect(jsonPath("$._links", Matchers.hasKey(linkName)));
    }

    @Test
    void one_whenIdNotFoundInRepository_shouldReturn404() throws Exception {
        // GIVEN
        UUID id = UUID.randomUUID();
        Mockito.when(service.findById(id)).thenReturn(Optional.empty());

        // WHEN
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/characters/" + id);
        // THEN
        mockMvc.perform(request)
                .andExpect(status().isNotFound());
    }

    @ParameterizedTest
    @ValueSource(strings = {"self", "collection", "contents"})
    void create_shouldReturn201WithLocationForCreatedCharacter_andJsonRepresentationOfCharacterInBody(String linkName) throws Exception {
        // GIVEN
        Character test = new Character("Test");
        UUID expectedId = UUID.randomUUID();
        test.setId(expectedId);
        Mockito.when(service.createCharacter(any(Character.class))).thenReturn(test);
        CharacterDto dto = CharacterDto.Builder.newInstance().withName(test.getName()).build();

        // WHEN
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/characters")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(dto));

        // THEN
        mockMvc.perform(request)
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", Matchers.endsWith("/characters/" + expectedId)))
                .andExpect(jsonPath("$", notNullValue()))
                .andExpect(jsonPath("$.name", is("Test")))
                .andExpect(jsonPath("$._links", Matchers.hasKey(linkName)));

    }

    @ParameterizedTest
    @ValueSource(strings = {"self", "contents"})
    void all_shouldReturnAllCharacters(String linkName) throws Exception {
        //GIVEN
        Character test1 = new Character("Test1");
        Character test2 = new Character("Test2");
        Mockito.when(service.findAll()).thenReturn(Arrays.asList(test1, test2));

        //WHEN/THEN
        mockMvc.perform(MockMvcRequestBuilders.get("/characters"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.characterDtoList", Matchers.hasSize(2)))
                .andExpect(jsonPath("$._links", Matchers.hasKey(linkName)));
    }

    @TestConfiguration
    static class TestConfig {
        @Bean
        public CharacterModelAssembler characterModelAssembler() {
            return new CharacterModelAssembler();
        }

    }
}