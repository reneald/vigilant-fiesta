package be.leanderonline.cscg.api.types;

import be.leanderonline.cscg.model.characters.types.MongoTypeRepository;
import be.leanderonline.cscg.model.characters.types.Type;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(TypeController.class)
class TypeControllerTest {
    @Autowired
    MockMvc mockMvc;
    @MockBean
    private MongoTypeRepository repository;
    @Autowired
    private TypeModelAssembler assembler;

    @ParameterizedTest
    @ValueSource(strings = {"self", "collection", "contents"})
    void one_whenIdFoundInRepository_thenShouldReturnOkWithCorrectType(String linkName) throws Exception {
        // GIVEN
        Type warrior = new Type(1, "Warrior", 10, 10, 7, 6, 1);
        Mockito.when(repository.findById(1)).thenReturn(Optional.of(warrior));

        // WHEN/THEN
        mockMvc.perform(MockMvcRequestBuilders.get("/types/1")
                        .contentType("application/hal+json"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").doesNotExist())
                .andExpect(jsonPath("$.name", is("Warrior")))
                .andExpect(jsonPath("$.might", is(10)))
                .andExpect(jsonPath("$.speed", is(10)))
                .andExpect(jsonPath("$.intellect", is(7)))
                .andExpect(jsonPath("$.additionalPoints", is(6)))
                .andExpect(jsonPath("$.effort", is(1)))
                .andExpect(jsonPath("$._links", Matchers.hasKey(linkName)));
    }

    @Test
    void one_whenIdNotFoundInRepository_shouldReturn404() throws Exception {
        // GIVEN
        Mockito.when(repository.findById(1)).thenReturn(Optional.empty());

        // WHEN/THEN
        mockMvc.perform(MockMvcRequestBuilders.get("/types/1"))
                .andExpect(status().isNotFound());
    }

    @ParameterizedTest
    @ValueSource(strings = {"self", "contents"})
    void all_shouldReturnAllTypes_andContainCorrectLinks(String linkName) throws Exception {
        //GIVEN
        Type warrior = new Type(1, "Warrior", 10, 10, 7, 6, 1);
        Type adept = new Type(2, "Adept", 10, 10, 7, 6, 1);
        Mockito.when(repository.findAll()).thenReturn(Arrays.asList(warrior, adept));

        //WHEN/THEN
        mockMvc.perform(MockMvcRequestBuilders.get("/types"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$._embedded.typeDtoList", Matchers.hasSize(2)))
                .andExpect(jsonPath("$._links", Matchers.hasKey(linkName)));
    }

    @TestConfiguration
    static class TestConfig {
        @Bean
        public TypeModelAssembler typeModelAssembler() {
            return new TypeModelAssembler();
        }

    }
}