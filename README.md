# Cypher System Character Generator
This is a back end application that provides a character generator for the Cypher System by Monte Cook Games.

**This is in no way finished, I'm just getting started setting everything up.** The MVP and further releases will be published on the Releases tab ([https://codeberg.org/reneald/vigilant-fiesta/releases](https://codeberg.org/reneald/vigilant-fiesta/releases)).

## Navigating the API
This API implements [HATEOAS](https://restfulapi.net/hateoas/) using HAL. This means that each resource will provide links to other relevant resources, so you can navigate the API without knowing the exact URIs that it uses, except the first one.
This also means that you don't have to hard code any URIs (again, except the first one) unless you really want to (but why would you?).
Provided that CSCG is running on localhost:8080, the base URI is `localhost:8080/api/links`. A GET request to that URI will provide an `application/hal+json` response with the following structure:
```
{
    "_embedded":
    {
        "stringList":
        ["This resource's links section contains links to all collections that this API provides."]
    },
    "_links":
    {
        "self":
        {
            "href": "http://localhost:8080/links/"
        },
        "characters":
        {
            "href": "http://localhost:8080/characters/all"
        },
        "types":
        {
            "href": "http://localhost:8080/types/all"
        }
    }
}
```

Note the `_links` section. it contains links to related resources (*rels*), with a name and a URI. As you can see, you can request a list of all available Types by following the URI defined in the `types` rel. A GET call to that endpoint would in turn also contain a `_links` section.
  * As per the HAL specification, there will always be a `self` rel containing a URI to the endpoint that resulted in the response that you got.
  * CSCG will always provide a rel of type `contents` which points back to the base URI as well.
  * If an endpoint provides you with a collection of items, they will be in a section called `_embedded`. Each item will also have a `_links` section and possibly an `_embedded` section.
  * The base URI is an exception to this last rule, because we are only interested in the `_links`, not in the `_embedded` section.

By following the rels in each response's `_links` section, you can find your way through the API without ever knowing the URI's that you're using.
Multiple frameworks exist that make navigating a HAL API (even) easier. Mike Kelly has collected several of them on this repository: [https://github.com/mikekelly/hal_specification/wiki/Libraries](https://github.com/mikekelly/hal_specification/wiki/Libraries). For JavaScript, [Traverson](https://www.npmjs.com/package/traverson) seems robust and is (as of November 30 2022) still being maintained.

## Cypher System Open License
This product is an independent production and is not affiliated with Monte Cook Games, LLC. It is published
under the Cypher System Open License, found at [http://csol.montecookgames.com](http://csol.montecookgames.com).

CYPHER SYSTEM and its logo are trademarks of Monte Cook Games, LLC in the U.S.A. and other countries. All Monte Cook Games characters and character names, and the distinctive likenesses thereof, are trademarks of Monte Cook Games, LLC.
