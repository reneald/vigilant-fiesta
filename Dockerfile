FROM bellsoft/liberica-openjdk-alpine:17
MAINTAINER leanderonline.be
COPY target/cscg-0.0.5.jar cscg-0.0.5.jar
ENTRYPOINT ["java","-jar","/cscg-0.0.5.jar"]